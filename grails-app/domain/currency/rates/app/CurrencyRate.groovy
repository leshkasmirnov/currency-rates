package currency.rates.app

import java.time.LocalDate
import java.time.ZoneId

class CurrencyRate {

    LocalDate date
    Currency currency
    BigDecimal rate

    static constraints = {
        date blank: false
        currency blank: false
        rate blank: false, scale: 4
    }

    long getDateMillis() {
        date.atStartOfDay(ZoneId.of('UTC')).toInstant().toEpochMilli()
    }

}
