package currency.rates.app

class Currency {

    String name
    String code
    boolean active

    static constraints = {
        name blank: false, size: 1..255
        code blank: false, size: 3..8, unique: true
    }
}
