<%@ page import="java.time.format.DateTimeFormatter" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Currency rates</title>
</head>

<body>

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#table">Таблица</a></li>
    <li><a data-toggle="tab" href="#chart">Графики</a></li>
</ul>

<div class="tab-content">
    <div id="table" class="tab-pane fade in active">
        <table class="table-bordered">
            <thead>
            <tr>
                <th>Дата</th>
                <g:each in="${currencies}" var="currency">
                    <th>${currency.code}</th>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:each in="${rates}" var="rate">
                <tr>
                    <td>${rate.key.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))}</td>
                    <g:each in="${rate.value}" var="currRate">
                        <td>
                            <g:if test="${minRates[currRate.currency.code].rate == currRate.rate}">
                                <span class="minRate">${currRate.rate}</span>
                            </g:if>
                            <g:elseif test="${maxRates[currRate.currency.code].rate == currRate.rate}">
                                <span class="maxRate">${currRate.rate}</span>
                            </g:elseif>
                            <g:else>
                                <span>${currRate.rate}</span>
                            </g:else>
                        </td>
                    </g:each>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

    <div id="chart" class="tab-pane fade">
        <g:render template="chart" model="[rates: ratesByCode]"/>
    </div>
</div>

</body>
</html>
