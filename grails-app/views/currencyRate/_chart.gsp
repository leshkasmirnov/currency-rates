<script>
    $(document).ready(function () {
        Highcharts.chart('container', {

            title: {
                text: 'Currency rates'
            },

            subtitle: {
                text: 'Source: cbr.ru'
            },

            xAxis: {
                type: 'datetime',
                labels: {
                    formatter: function () {
                        return Highcharts.dateFormat("%b %e", this.value);
                    }
                }
            },

            yAxis: {
                title: {
                    text: 'RUB'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    }
                }
            },

            series: [
                <g:each var="rate" in="${rates}">
                {
                    name: '${rate.key}',
                    data: [
                        <g:each var="curRate" in="${rate.value}">
                            [${curRate.dateMillis}, ${curRate.rate}],
                        </g:each>
                    ]
                },
                </g:each>
            ],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    });
</script>
<div id="container"></div>