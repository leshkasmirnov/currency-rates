package currency.rates.app

import grails.gorm.transactions.Transactional

@Transactional
class CurrencyService {

    def getActive() {
        Currency.findAllByActive(true).sort { it.code }
    }
}
