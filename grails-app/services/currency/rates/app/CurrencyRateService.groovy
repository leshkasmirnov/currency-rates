package currency.rates.app

import currency.rates.app.provider.CurrencyRatesProvider
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Value

import java.time.LocalDate

@Transactional
class CurrencyRateService {

    @Value('${currency.rate.days}')
    int countDays

    CurrencyRatesProvider currencyRatesProvider

    def refreshRates() {
        List<Currency> currencies = Currency.findAllByActive(true)
        if (!currencies) {
            return
        }

        LocalDate now = LocalDate.now()
        for (long i = countDays - 1; i >= 0; i--) {
            LocalDate offsetDate = now.minusDays(i)
            if (!CurrencyRate.findByDate(offsetDate)) {
                refreshRatesByDate(currencies, offsetDate)
            }
        }
    }

    private void refreshRatesByDate(List<Currency> currencies, LocalDate date) {
        def rates = currencyRatesProvider.getRates(currencies, date)
        CurrencyRate.saveAll(rates)
    }

    def getRates() {
        Map<LocalDate, List<CurrencyRate>> rates = getAll().groupBy { it.date }
        rates.each { r -> r.value.sort { it.currency.code } }
    }

    def getRatesByCode() {
        Map<String, List<CurrencyRate>> rates = getAll().groupBy { it.currency.code }
        rates.each { r -> r.value.sort { it.currency.code } }
    }

    private List<CurrencyRate> getAll() {
        CurrencyRate.findAllByDateGreaterThan(LocalDate.now().minusDays(countDays))
    }

    def getMinRates() {
        def rates = getAll().groupBy { it.currency.code }

        def result = [:]
        rates.each { rate -> result[rate.key] = rate.value.min { it.rate } }

        result
    }

    def getMaxRates() {
        def rates = getAll().groupBy { it.currency.code }

        def result = [:]
        rates.each { rate -> result[rate.key] = rate.value.max { it.rate } }

        result
    }
}
