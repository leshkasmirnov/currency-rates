package currency.rates.app

class CurrencyRateController {

    CurrencyService currencyService
    CurrencyRateService currencyRateService

    def index() {
        respond([
                currencies: currencyService.active,
                rates: currencyRateService.rates,
                ratesByCode: currencyRateService.ratesByCode,
                minRates: currencyRateService.minRates,
                maxRates: currencyRateService.maxRates
        ])
    }
}
