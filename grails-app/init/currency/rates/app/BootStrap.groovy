package currency.rates.app

class BootStrap {

    CurrencyRateService currencyRateService

    def init = { servletContext ->
        new Currency(name: "Доллар США", code: "USD", active: true).save()
        new Currency(name: "Евро", code: "EUR", active: true).save()
        //new Currency(name: "Австралийский доллар", code: "AUD", active: true).save()

        currencyRateService.refreshRates()
    }
    def destroy = {
    }
}
