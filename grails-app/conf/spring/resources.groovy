import currency.rates.app.provider.CbrCurrencyRatesProvider

// Place your Spring DSL code here
beans = {
    currencyRatesProvider(CbrCurrencyRatesProvider)
}
