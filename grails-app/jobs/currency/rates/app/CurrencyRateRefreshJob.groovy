package currency.rates.app

class CurrencyRateRefreshJob {

    CurrencyRateService currencyRateService

    static triggers = {
        cron cronExpression: '0 0 0 * * ?'
    }

    def execute() {
        currencyRateService.refreshRates()
    }
}
