package currency.rates.app

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

import java.time.LocalDate

class CurrencyRateControllerSpec extends Specification implements ControllerUnitTest<CurrencyRateController> {
    LocalDate now = LocalDate.now()
    LocalDate beforeNow = now.minusDays(1)

    def setup() {
    }

    def cleanup() {
    }

    void "test index"() {
        given:

        def eur = new Currency(name: 'Euros', code: 'EUR', active: true)
        def usd = new Currency(name: 'Dollars USA', code: 'USD', active: true)

        def sampleCurrencies = [eur, usd]
        def sampleRates = [new CurrencyRate(date: beforeNow, currency: eur, rate: new BigDecimal("76.66")),
                           new CurrencyRate(date: beforeNow, currency: usd, rate: new BigDecimal("66.76")),
                           new CurrencyRate(date: now, currency: eur, rate: new BigDecimal("75.65")),
                           new CurrencyRate(date: now, currency: usd, rate: new BigDecimal("65.75"))]

        controller.currencyService = Stub(CurrencyService) {
            getActive() >> sampleCurrencies
        }
        controller.currencyRateService = Stub(CurrencyRateService) {
            getRates() >> sampleRates
        }

        when:
        controller.index()

        then:
        model.currencies
        model.rates
        model.ratesByCode
        model.minRates
        model.maxRates
    }
}
