package currency.rates.app

import currency.rates.app.provider.CurrencyRatesProvider
import grails.test.hibernate.HibernateSpec
import grails.testing.services.ServiceUnitTest

import java.time.LocalDate

class CurrencyRateServiceSpec extends HibernateSpec implements ServiceUnitTest<CurrencyRateService> {

    CurrencyRatesProvider currencyRatesProvider = Mock()

    def eur = new Currency(name: 'Euros', code: 'EUR', active: true)
    def usd = new Currency(name: 'Dollars USA', code: 'USD', active: true)

    LocalDate now = LocalDate.now()
    LocalDate beforeNow = now.minusDays(1)
    private String EUR_CODE = "EUR"
    private String USD_CODE = "USD"

    def setup() {
        service.currencyRatesProvider = this.currencyRatesProvider
        service.countDays = 2

        eur = eur.save()
        usd = usd.save()
    }

    void "test refreshRates"() {
        given:
        1 * currencyRatesProvider.getRates([eur, usd], now) >> [
                new CurrencyRate(date: now, currency: eur, rate: new BigDecimal("75.65")),
                new CurrencyRate(date: now, currency: usd, rate: new BigDecimal("65.75"))
        ]
        1 * currencyRatesProvider.getRates([eur, usd], beforeNow) >> [
                new CurrencyRate(date: beforeNow, currency: eur, rate: new BigDecimal("76.66")),
                new CurrencyRate(date: beforeNow, currency: usd, rate: new BigDecimal("66.76"))
        ]

        when:
        service.refreshRates()
        def currencyRates = CurrencyRate.findAll()

        then:
        currencyRates.size() == 4
        currencyRates[0].date == beforeNow
        currencyRates[0].currency.code == 'EUR'
        currencyRates[0].rate == new BigDecimal("76.66")

        currencyRates[1].date == beforeNow
        currencyRates[1].currency.code == 'USD'
        currencyRates[1].rate == new BigDecimal("66.76")

        currencyRates[2].date == now
        currencyRates[2].currency.code == 'EUR'
        currencyRates[2].rate == new BigDecimal("75.65")

        currencyRates[3].date == now
        currencyRates[3].currency.code == 'USD'
        currencyRates[3].rate == new BigDecimal("65.75")
    }

    void "test getRates"() {
        when:
        saveRates()

        def rates = service.getRates()

        then:
        rates.size() == 2
        rates.keySet()[0] == beforeNow
        rates[beforeNow].size() == 2
        rates[beforeNow][0].currency.code == EUR_CODE
        rates[beforeNow][0].rate == new BigDecimal("76.66")
        rates[beforeNow][1].currency.code == USD_CODE
        rates[beforeNow][1].rate == new BigDecimal("66.76")

        rates.keySet()[1] == now
        rates[now].size() == 2
        rates[now][0].currency.code == EUR_CODE
        rates[now][0].rate == new BigDecimal("75.65")
        rates[now][1].currency.code == USD_CODE
        rates[now][1].rate == new BigDecimal("65.75")
    }

    void "test getRatesByCode"() {
        when:
        saveRates()

        def rates = service.getRatesByCode()

        then:
        rates.size() == 2

        rates.keySet()[0] == EUR_CODE
        rates[EUR_CODE].size() == 2
        rates[EUR_CODE][0].date == beforeNow
        rates[EUR_CODE][0].rate == new BigDecimal("76.66")
        rates[EUR_CODE][1].date == now
        rates[EUR_CODE][1].rate == new BigDecimal("75.65")

        rates.keySet()[1] == USD_CODE
        rates[USD_CODE].size() == 2
        rates[USD_CODE][0].date == beforeNow
        rates[USD_CODE][0].rate == new BigDecimal("66.76")
        rates[USD_CODE][1].date == now
        rates[USD_CODE][1].rate == new BigDecimal("65.75")
    }

    void "test getMinRates"() {
        when:
        saveRates()

        def rates = service.getMinRates()

        then:
        rates.size() == 2
        rates[EUR_CODE].rate == new BigDecimal("75.65")
        rates[USD_CODE].rate == new BigDecimal("65.75")
    }

    void "test getMaxRates"() {
        when:
        saveRates()

        def rates = service.getMaxRates()

        then:
        rates.size() == 2
        rates[EUR_CODE].rate == new BigDecimal("76.66")
        rates[USD_CODE].rate == new BigDecimal("66.76")
    }

    private void saveRates() {
        CurrencyRate.saveAll(
                new CurrencyRate(date: beforeNow, currency: eur, rate: new BigDecimal("76.66")),
                new CurrencyRate(date: beforeNow, currency: usd, rate: new BigDecimal("66.76")),
                new CurrencyRate(date: now, currency: eur, rate: new BigDecimal("75.65")),
                new CurrencyRate(date: now, currency: usd, rate: new BigDecimal("65.75"))
        )
    }
}
