package currency.rates.app

import grails.test.hibernate.HibernateSpec
import grails.testing.services.ServiceUnitTest

class CurrencyServiceSpec extends HibernateSpec implements ServiceUnitTest<CurrencyService> {

    def setup() {
        Currency.saveAll(
                new Currency(name: 'Euros', code: 'EUR', active: true),
                new Currency(name: 'Dollars USA', code: 'USD', active: true),
                new Currency(name: 'Dollars Australian', code: 'AUD', active: false)
        )
    }

    def cleanup() {
    }

    void "test getActive"() {
        given: 'Currencies'

        when: 'service is called'
        def active = service.active

        then: 'active sorted currencies are reverted'
        active.size() == 2
        active[0].code == 'EUR'
        active[1].code == 'USD'
    }
}
