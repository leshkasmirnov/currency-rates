package currency.rates.app.provider

import currency.rates.app.Currency
import currency.rates.app.CurrencyRate
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import org.springframework.beans.factory.annotation.Value

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class CbrCurrencyRatesProvider implements CurrencyRatesProvider {

    @Value('${currency.provider.url}')
    String baseUrl

    @Override
    List<CurrencyRate> getRates(List<Currency> currencies, LocalDate date) {
        String queryDate = date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
        List<String> currencyCodes = currencies.collect { it.code }
        Map<String, Currency> currencyMap = currencies.collectEntries {
            [(it.code): it]
        }

        Document doc = Jsoup.connect("${baseUrl}/?date_req=${queryDate}").get()
        Element coursesTable = doc.selectFirst("table.data")
        Elements rows = coursesTable.getElementsByTag("tr")
        List<CurrencyRate> rates = rows.findAll { row ->
            row.getElementsByTag("td").any { currencyCodes.contains(it.html()) }
        }.collect { row ->
            def columns = row.getElementsByTag("td")
            new CurrencyRate(
                    date: date,
                    currency: currencyMap[columns[1].html()],
                    rate: new BigDecimal(columns[4].html().replace(',', '.'))
            )
        }
        rates
    }
}
