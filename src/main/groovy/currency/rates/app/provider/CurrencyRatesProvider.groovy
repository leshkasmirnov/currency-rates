package currency.rates.app.provider

import currency.rates.app.Currency
import currency.rates.app.CurrencyRate

import java.time.LocalDate

interface CurrencyRatesProvider {

    /**
     * Return currencies for date.
     *
     * @param currencies
     * @param date
     * @return
     */
    List<CurrencyRate> getRates(List<Currency> currencies, LocalDate date)

}